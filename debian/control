Source: ruby-httparty
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Cédric Boutillier <boutil@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake,
               ruby,
               help2man,
               ruby-fakeweb,
               ruby-mime-types (>= 3.0),
               ruby-multi-json,
               ruby-multi-xml,
               ruby-rspec (>= 3~),
               ruby-simplecov,
               ruby-webmock,
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-httparty.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-httparty
Homepage: https://github.com/jnunemaker/httparty
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-httparty
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends},
         ruby-mime-types (>= 3.0),
         ruby-multi-json,
         ruby-multi-xml
Breaks: ruby-gitlab (<< 4.16.1~)
Description: quick web service consumption from any Ruby class
 HTTParty is a Ruby library to build easily classes that can use
 Web-based APIs and related services. At its simplest, the
 HTTParty module is included within a class, which gives the class a
 "get" method that can retrieve data over HTTP. Further directives,
 however, instruct HTTParty to parse results (XML, JSON, and so on),
 define base URIs for the requests, and define HTTP authentication
 information.
